package com.dosenku.app;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.dosenku.app.api.ActivityApi;
import com.dosenku.app.api.SettingApi;
import com.dosenku.app.model.ActivityModel;
import com.dosenku.app.model.SettingModel;
import com.dosenku.app.utils.Helpers;

import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LocationUpdater extends Service
        implements LocationListener {

    private SharedPreferences spService, spData;
    private Context context;
    private float lat1, lat2, lng1, lng2;
    private int dosenId;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    boolean canGetLocation = false;

    Location location; // location
    float latitude; // latitude
    float longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public LocationUpdater() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        spService = getSharedPreferences("dosenkuService", MODE_PRIVATE);
        spData = getSharedPreferences("dosenkuData", MODE_PRIVATE);

        context = getApplicationContext();

        dosenId = spData.getInt("userId", 0);

        Log.e("start service", "Starting using user: " + dosenId);

        updateLocation();
        scheduleUpdate();

        Intent intent = new Intent(context, Home.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Updating your location in background...")
                .setContentIntent(pendingIntent)
                .build();

        startForeground(123456, notification);

        if (spService.contains("lat1")) {
            lat1 = spService.getFloat("lat1", 0);
            lng1 = spService.getFloat("lng1", 0);
            lat2 = spService.getFloat("lat2", 0);
            lng2 = spService.getFloat("lng2", 0);
        }

        getValidCoordinate();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void scheduleUpdate() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                updateLocation();
                scheduleUpdate();
            }
        }, 30000);


    }

    private void updateLocation() {
        getLocation();
        Log.e("latitude", latitude + "   " + lat1 + " --- " + lat2 + String.valueOf(latitude >= lat1 && latitude <= lat2));
        Log.e("longitude", longitude + "   " + lng1 + " --- " + lng2 + String.valueOf(longitude >= lng1 && longitude <= lng2));

        if (latitude >= lat1 && latitude <= lat2 && longitude >= lng1 && longitude <= lng2) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String apiUrl = getResources().getString(R.string.api_url);
                    RestAdapter adapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
                    ActivityApi api = adapter.create(ActivityApi.class);

                    api.send(dosenId, Helpers.getCurrentDateTime(), latitude, longitude, new Callback<ActivityModel>() {
                        @Override
                        public void success(ActivityModel activityModel, Response response) {

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("location", "Location update error");
                            retryUpdateLocation();
                        }

                    });

                }
            }, 1000);
        } else {
            Log.e("location", "Out of range! Current: " + latitude + "," + longitude);
        }

    }

    private void retryUpdateLocation() {
        Log.e("location", "Retrying location update");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLocation();
            }
        }, 5000);
    }

    private void getValidCoordinate() {
        String apiUrl = getResources().getString(R.string.api_url);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();

        SettingApi api = adapter.create(SettingApi.class);
        Log.e("validCoordinate", "Getting valid coordinate");
        api.getSetting("valid-coordinate", new Callback<SettingModel>() {
            @Override
            public void success(SettingModel settingModel, Response response) {
                String value = settingModel.getValue();
                StringTokenizer tokenizer = new StringTokenizer(value, "|");

                lat1 = Float.valueOf(tokenizer.nextToken());
                lng1 = Float.valueOf(tokenizer.nextToken());
                lat2 = Float.valueOf(tokenizer.nextToken());
                lng2 = Float.valueOf(tokenizer.nextToken());

                if (lat1 > lat2) {
                    float temp = lat1;
                    lat1 = lat2;
                    lat2 = temp;
                }

                if (lng1 > lng2) {
                    float temp = lng1;
                    lng1 = lng2;
                    lng2 = lng1;
                }

                //Log.e("Valid coord", lat1 + "," + lng1 + "  --  " + lat2 + "," + lng2);
                SharedPreferences.Editor editor = spService.edit();
                editor.putFloat("lat1", lat1);
                editor.putFloat("lng1", lng1);
                editor.putFloat("lat2", lat2);
                editor.putFloat("ln21", lng2);
                editor.apply();

                Log.e("validCoordinate", lat1 + "," + lng1 + " -- " + lat2 + "," + lng2);
            }

            @Override
            public void failure(RetrofitError error) {
                getValidCoordinate();
                Log.e("validCoordinate", "Failed to get valid coordinate");
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = (float)location.getLatitude();
        longitude = (float)location.getLongitude();
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private Location getLocation() {
        Log.e("getLocation", "Getting location");
        try {
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isNetworkEnabled && !isGPSEnabled) {
                //no provider
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider

                if (isNetworkEnabled) {
                    Log.e("getLocation", "Getting network location");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = (float)location.getLatitude();
                            longitude = (float)location.getLongitude();

                            Log.e("getLocation", "Network location: " + latitude + "," + longitude);
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    Log.e("getLocation", "Getting GPS location");
                    if (location == null) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            //nothing
                        }
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                0,
                                0, this);
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = (float)location.getLatitude();
                                longitude = (float)location.getLongitude();
                                Log.e("getLocation", "GPS location: " + latitude + "," + longitude);
                            }
                        }
                    }
                }
            }
            Log.e("getLocation", latitude + "," + longitude);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("getLocation", "Failed to get location!");
        }

        return location;
    }
}
