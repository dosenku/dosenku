package com.dosenku.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.*;
import android.widget.*;

import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.utils.Image;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LecturerListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LecturerListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LecturerListFragment extends Fragment
    implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private View rootView;
    private SharedPreferences settings;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LecturerListFragment.
     */
    public static LecturerListFragment newInstance() {
        LecturerListFragment fragment = new LecturerListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public LecturerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        Configuration config = this.getActivity().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            this.getActivity().getResources().updateConfiguration(config, this.getActivity().getResources().getDisplayMetrics());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_lecturer_list, container, false);


        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_lecturer);

        //auto start refreshing
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_1, R.color.refresh_2, R.color.refresh_3, R.color.refresh_4, R.color.refresh_5, R.color.refresh_6);
        swipeRefreshLayout.setOnRefreshListener(this);


        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        this.setLecturers();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setLecturers() {
        String apiUrl = this.getResources().getString(R.string.api_url);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();

        LecturerApi api = restAdapter.create(LecturerApi.class);

        api.getLecturers(new Callback<ArrayList<LecturerModel>>() {
            @Override
            public void success(ArrayList<LecturerModel> lecturerModels, Response response) {
                if (LecturerListFragment.this.isVisible()) {
                    // Each row in the list stores country name, currency and flag
                    ArrayList<LecturerObject> lecturerList = new ArrayList<LecturerObject>();


                    for (LecturerModel obj : lecturerModels) {
                        if (obj.getLastSeen() == 0) {
                            lecturerList.add(new LecturerObject(obj.getUserId(), obj.getName(), obj.getPhoto(), true));
                        }
                    }

                    for (LecturerModel obj : lecturerModels) {
                        if (obj.getLastSeen() != 0) {
                            lecturerList.add(new LecturerObject(obj.getUserId(), obj.getName(), obj.getPhoto(), false));
                        }
                    }

                    LecturerAdapter adapter = new LecturerAdapter(LecturerListFragment.this, lecturerList);

                    GridView gridView = (GridView) getView().findViewById(R.id.gridview_lecturer);
                    gridView.setAdapter(adapter);


                    AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            LecturerObject obj = (LecturerObject) parent.getItemAtPosition(position);
                            Activity act = LecturerListFragment.this.getActivity();
                            Intent intent = new Intent(act, LecturerInfo.class);

                            intent.putExtra("lecturerId", obj.getId());
                            intent.putExtra("lecturerName", obj.getName());

                            act.startActivity(intent);
                        }
                    };

                    gridView.setOnItemClickListener(listener);

                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(getView(), R.string.error_fetch, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Refresh", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                swipeRefreshLayout.setRefreshing(true);
                                LecturerListFragment.this.setLecturers();

                            }
                        })
                        .show();
                swipeRefreshLayout.setRefreshing(false);
                Log.e("error list", error.toString()+" "+error.getMessage());
            }
        });

    }

    @Override
    public void onRefresh() {
        this.setLecturers();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //public void onFragmentInteraction(Uri uri);
    }

    private class LecturerObject {
        private int id;
        private String name;
        private String image;
        private boolean isOnline;

        LecturerObject(int id, String name, String image, boolean isOnline) {
            this.id = id;
            this.name = name;
            this.image = image;
            this.isOnline = isOnline;
        }


        public String getName() {
            return this.name;
        }

        public String getImage() {
            return this.image;
        }

        public int getId() {
            return this.id;
        }

        public boolean getIsOnline() { return this.isOnline; }
    }

    private class LecturerAdapter extends BaseAdapter {
        private ArrayList<LecturerObject> lecturerList;
        private Fragment parentFragment;
        private LayoutInflater inflater;

        LecturerAdapter(Fragment parent, ArrayList<LecturerObject> list) {
            parentFragment = parent;
            lecturerList = list;
            inflater = (LayoutInflater) parentFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return lecturerList.size();
        }

        @Override
        public Object getItem(int position) {
            return lecturerList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = inflater.inflate(R.layout.item_lecturer, null);
            }

            CircleImageView image = (CircleImageView) view.findViewById(R.id.item_lecturer_image);
            TextView name = (TextView) view.findViewById(R.id.item_lecturer_name);
            LecturerObject obj = lecturerList.get(position);


            name.setText(obj.getName());

            Image.setImage(parentFragment.getActivity(), image, obj.getImage());


            if (!obj.getIsOnline()) {
                //function to greyscale the image
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);  //0 means grayscale
                ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
                image.setColorFilter(cf);
            }

            return view;
        }
    }


}
