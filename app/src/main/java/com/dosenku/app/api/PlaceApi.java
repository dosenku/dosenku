package com.dosenku.app.api;

import com.dosenku.app.model.PlaceModel;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by charl on 1/14/2016.
 */
public interface PlaceApi {
    @GET("/place")
    void get(Callback<ArrayList<PlaceModel>> response);
}
