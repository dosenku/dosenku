package com.dosenku.app.api;

import com.dosenku.app.model.LecturerModel;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * Created by charl on 11/15/2015.
 */
public interface LecturerApi {
    @GET("/dosen/{dosen}")
    void getLecturerDetail(@Path("dosen") String user, Callback<LecturerModel> response);

    @GET("/dosen")
    void getLecturers(Callback<ArrayList<LecturerModel>> response);

    @Multipart
    @POST("/dosen")
    void createLecturer(
            @Part("user_id") int userId,
            @Part("name") String name,
            @Part("phone") String phone,
            @Part("email") String email,
            @Part("photo") TypedFile photo,
            @Part("password") String password,
            Callback<LecturerModel> response);

    @FormUrlEncoded
    @POST("/dosen/{user_id}")
    void updateData(
            @Path("user_id") int userId,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("password") String password,
            Callback<LecturerModel> response);

    @Multipart
    @POST("/dosen/{user_id}")
    void updatePhoto(@Path("user_id") int userId, @Part("photo") TypedFile photo, Callback<LecturerModel> response);
}
