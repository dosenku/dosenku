package com.dosenku.app.api;

import com.dosenku.app.model.ActivityModel;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by charl on 1/3/2016.
 */
public interface ActivityApi {
    @FormUrlEncoded
    @POST("/activity")
    void send(@Field("dosen_id") int dosenId, @Field("time") String time, @Field("latitude") float latitude, @Field("longitude") float longitude, Callback<ActivityModel> response);

    @FormUrlEncoded
    @POST("/activity")
    void sendWithPlace(@Field("dosen_id") int dosenId, @Field("time") String time, @Field("latitude") float latitude, @Field("longitude") float longitude, @Field("place_id") int placeId, Callback<ActivityModel> response);

}
