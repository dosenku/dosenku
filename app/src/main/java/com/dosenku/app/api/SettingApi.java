package com.dosenku.app.api;

import com.dosenku.app.model.SettingModel;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by charl on 1/3/2016.
 */
public interface SettingApi {
    @GET("/setting/{id}")
    void getSetting(@Path("id") String id, Callback<SettingModel> response);

}
