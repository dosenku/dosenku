package com.dosenku.app.api;

import com.dosenku.app.model.UserLoginModel;
import com.dosenku.app.model.UserModel;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by charl on 11/22/2015.
 */
public interface UserApi {
    @FormUrlEncoded
    @POST("/user/search")
    public void login(@Field("username") String username, @Field("password") String password, Callback<UserLoginModel> response);

}
