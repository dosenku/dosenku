package com.dosenku.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.utils.Image;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LecturerInfo extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private String lecturerName;
    private int lecturerId;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturer_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }


        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);

        //ambil dari intent
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            lecturerName = extras.getString("lecturerName");
            lecturerId = extras.getInt("lecturerId");
        } else {
            lecturerName = savedInstanceState.getString("lecturerName");
            lecturerId = savedInstanceState.getInt("lecturerId");
        }

        //set data dr intent
        this.getSupportActionBar().setTitle(lecturerName);
        TextView textViewLecturerName = (TextView) this.findViewById(R.id.textview_lecturer_name);
        textViewLecturerName.setText(lecturerName);


        swipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.swipe_refresh_lecturer_info);
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_1, R.color.refresh_2, R.color.refresh_3, R.color.refresh_4, R.color.refresh_5, R.color.refresh_6);
        swipeRefreshLayout.setOnRefreshListener(this);


        this.setLecturer();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("lecturerName", this.lecturerName);
        savedInstanceState.putInt("lecturerId", this.lecturerId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.nav_editprofile:
                Intent intent = new Intent(this, EditAccount.class);
                startActivity(intent);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SharedPreferences sharedPreferences = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);

        if (sharedPreferences.contains("userId")) {
            if (sharedPreferences.getInt("userId", 0) == lecturerId) {
                getMenuInflater().inflate(R.menu.lecturer_info, menu);
            }
        }

        return true;
    }

    public void setLecturer() {
        //get the data
        String apiUrl = getResources().getString(R.string.api_url);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
        LecturerApi api = restAdapter.create(LecturerApi.class);

        api.getLecturerDetail(String.valueOf(lecturerId), new Callback<LecturerModel>() {
            @Override
            public void success(LecturerModel lecturerModel, Response response) {
                //hide the progressbar
                swipeRefreshLayout.findViewById(R.id.container_progress).setVisibility(View.GONE);
                swipeRefreshLayout.findViewById(R.id.container_lecturer_info).setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);

                final String lecturerPhone = lecturerModel.getPhone();
                final String lecturerEmail = lecturerModel.getEmail();
                String lecturerImage = lecturerModel.getPhoto();
                int lastSeen = lecturerModel.getLastSeen();

                //set phone
                TextView textViewPhone = (TextView) swipeRefreshLayout.findViewById(R.id.textview_lecturer_phone);
                textViewPhone.setText(lecturerPhone);

                //set email
                TextView textViewEmail = (TextView) swipeRefreshLayout.findViewById(R.id.textview_lecturer_email);
                textViewEmail.setText(lecturerEmail);


                //set photo
                ImageView image = (ImageView) swipeRefreshLayout.findViewById(R.id.imageview_lecturer_image);
                Image.setImage(LecturerInfo.this, image, lecturerImage);

                //set phone button action
                ImageButton buttonPhone = (ImageButton) swipeRefreshLayout.findViewById(R.id.button_phone);
                buttonPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + lecturerPhone));
                        startActivity(intent);
                    }
                });

                //set message button action
                ImageButton buttonMessage = (ImageButton) swipeRefreshLayout.findViewById(R.id.button_messaging);
                buttonMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + lecturerPhone));
                        startActivity(smsIntent);

                    }
                });

                //set email button action
                ImageButton buttonEmail = (ImageButton) swipeRefreshLayout.findViewById(R.id.button_email);
                buttonEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + lecturerEmail));
                        startActivity(emailIntent);
                    }
                });


                //set last seen
                String strLastSeen = getResources().getString(R.string.last_seen_on) + " ";
                if (lastSeen == -1) {
                    strLastSeen += getResources().getString(R.string.never);
                } else if (lastSeen == 0) {
                    strLastSeen = getResources().getString(R.string.online);
                    TextView tvPlace = (TextView) swipeRefreshLayout.findViewById(R.id.textview_place);
                    tvPlace.setText(lecturerModel.getPlace());
                } else {
                    strLastSeen += lastSeen + " " + getResources().getString(R.string.minutes_ago);
                }
                TextView tvLastSeen = (TextView) swipeRefreshLayout.findViewById(R.id.textview_last_seen);
                tvLastSeen.setText(strLastSeen);
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(swipeRefreshLayout, R.string.error_fetch, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Refresh", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LecturerInfo.this.setLecturer();
                                swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                    }
                                });
                            }
                        })
                        .show();
                swipeRefreshLayout.findViewById(R.id.container_progress).setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        this.setLecturer();
    }
}
