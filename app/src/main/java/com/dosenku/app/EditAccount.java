package com.dosenku.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.utils.Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class EditAccount extends AppCompatActivity {
    private int userId;
    private String name, phone, email, photo, photoNow;
    private SharedPreferences sharedPreferences;
    private static final int IMAGE_REQUEST_CODE = 1;
    private ImageView ivPhoto;
    private EditText etName, etPhone, etEmail, etPassword, etConfirmPassword;
    private View rootView;
    private ProgressDialog pd;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rootView = findViewById(R.id.rootview);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);

        sharedPreferences = getSharedPreferences("dosenkuData", MODE_PRIVATE);

        userId = sharedPreferences.getInt("userId", 0);
        name = sharedPreferences.getString("name", "");
        email = sharedPreferences.getString("email", "");
        phone = sharedPreferences.getString("phone", "");
        photo = sharedPreferences.getString("photo", "");
        photoNow = photo;


        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        Image.setImage(this, ivPhoto, photo);

        etName = (EditText) findViewById(R.id.et_name);
        etName.setText(name);

        etPhone = (EditText) findViewById(R.id.et_phone);
        etPhone.setText(phone);

        etEmail = (EditText) findViewById(R.id.et_email);
        etEmail.setText(email);


        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);

        Button btnPickPhoto = (Button) findViewById(R.id.btn_pick_photo);
        btnPickPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

                startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
            }
        });
    }



    private void saveData() {
        if (validateInput() && isChanged()) {
            pd = new ProgressDialog(EditAccount.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.setMessage(getResources().getString(R.string.dialog_save_changes));
            pd.setIndeterminate(true);
            pd.show();

            String apiUrl = getResources().getString(R.string.api_url);
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
            final LecturerApi api = restAdapter.create(LecturerApi.class);


            api.updateData(
                    userId,
                    etName.getText().toString(),
                    etPhone.getText().toString(),
                    etEmail.getText().toString(),
                    etPassword.getText().toString(),
                    new Callback<LecturerModel>() {
                        @Override
                        public void success(LecturerModel lecturerModel, Response response) {
                            if (!photo.equals(photoNow)) {
                                Log.e("hahaha", photoNow);
                                TypedFile filePhoto = null;
                                try {
                                    filePhoto = new TypedFile("multipart/form-data", new File(new URI(photoNow)));
                                } catch (URISyntaxException ex) {
                                    ex.printStackTrace();
                                }

                                api.updatePhoto(userId, filePhoto, new Callback<LecturerModel>() {
                                    @Override
                                    public void success(LecturerModel lecturerModel, Response response) {
                                        saveSuccess();
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("2nd", error.getMessage());
                                        saveFail();
                                    }
                                });
                            } else {
                                saveSuccess();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("first", error.getMessage());
                            saveFail();
                        }
                    }
            );
        }
    }

    private void saveSuccess() {
        pd.dismiss();
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void saveFail() {
        pd.dismiss();
        Snackbar.make(rootView, R.string.error_save, Snackbar.LENGTH_LONG).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                Uri selectedImageUri = data.getData();

                ivPhoto.setImageBitmap(null);
                try {
                    ivPhoto.setImageBitmap(Image.decodeUri(this, selectedImageUri, 200));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                photoNow = selectedImageUri.toString();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isChanged()) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_exit_no_save)
                    .setMessage(R.string.content_exit_no_save)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.nav_save:
                saveData();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    private boolean isChanged() {
        boolean changed = false;

        if (!etName.getText().toString().equals(name)) {
            changed = true;
        }

        if (!etPhone.getText().toString().equals(phone)) {
            changed = true;
        }

        if (!etEmail.getText().toString().equals(email)) {
            changed = true;
        }

        if (!etPassword.getText().toString().isEmpty()) {
            changed = true;
        }

        if (!etConfirmPassword.getText().toString().isEmpty()) {
            changed = true;
        }

        if (!photoNow.equals(photo)) {
            changed = true;
        }

        return changed;
    }

    private boolean validateInput() {
        boolean valid = true;

        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();


        if (name.isEmpty()) {
            etName.setError("Enter a name");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (phone.isEmpty()) {
            etPhone.setError("Enter a phone number");
            valid = false;
        } else {
            etPhone.setError(null);
        }

        if (email.isEmpty()) {
            etEmail.setError("Enter an email address");
            valid = false;
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                etEmail.setError(null);
            } else {
                etEmail.setError("Enter a valid email address");
                valid = false;
            }
        }


        if (!password.equals(confirmPassword)) {
            etConfirmPassword.setError("Passwords do not match");
            valid = false;
        } else {
            etPassword.setError(null);
        }

        return valid;
    }
}
