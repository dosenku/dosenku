package com.dosenku.app.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.ImageView;

import com.dosenku.app.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by charl on 12/17/2015.
 */
public class Image {
    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    public static void setImage(Context context, ImageView iv, String path) {
        String imageUrl = context.getResources().getString(R.string.image_url);

        try {
            if (!(new URI(path)).isAbsolute()) {
                path = imageUrl + path;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Picasso picasso = Picasso.with(context);

        picasso.load(path)
                .placeholder(R.drawable.people_grey)
                .noFade()
                .into(iv);
    }
}
