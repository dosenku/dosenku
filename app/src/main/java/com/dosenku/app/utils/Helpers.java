package com.dosenku.app.utils;


import android.text.format.Time;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by charl on 12/27/2015.
 */
public class Helpers {
    public static long getCurrentTimestamp() {
        GregorianCalendar calendar = new GregorianCalendar();

        return calendar.getTimeInMillis()/1000;
    }

    public static String getCurrentDateTime() {
        GregorianCalendar calendar = new GregorianCalendar();

        String result = "";

        result = String.valueOf(calendar.get(GregorianCalendar.YEAR));

        result += "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1);

        result += "-" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

        result += " ";

        result += String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));

        result += ":" + String.valueOf(calendar.get(Calendar.MINUTE));

        result += ":" + String.valueOf(calendar.get(Calendar.SECOND));

        return result;
    }
}
