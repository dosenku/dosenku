package com.dosenku.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.utils.Image;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LecturerListFragment.OnFragmentInteractionListener{

    private View rootView;
    private SharedPreferences sharedPreferences,settings;
    private int userId;
    private int userRole;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        sharedPreferences = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);

        if (sharedPreferences.contains("userId")) {
            setContentView(R.layout.activity_home_after_login);
        } else {
            setContentView(R.layout.activity_home_before_login);
        }



        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        rootView = (View) this.findViewById(R.id.drawer_layout);





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (sharedPreferences.contains("userId")) {
            navigationView.inflateHeaderView(R.layout.nav_after_login);
            navigationView.inflateMenu(R.menu.drawer_after_login);


            userId = sharedPreferences.getInt("userId", 0);;
            userRole = sharedPreferences.getInt("userRole", 0);
            username = sharedPreferences.getString("username", "");

            if (userRole == 1) {
                setUserData();

            }


            //set tabs
            final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
            final TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
            adapter.addFragment(new LecturerHomeFragment(), getResources().getString(R.string.tab_lecturer_home));
            adapter.addFragment(new LecturerListFragment(), getResources().getString(R.string.tab_lecturer_list));
            viewPager.setAdapter(adapter);


            TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
            tabLayout.setupWithViewPager(viewPager);

            //set title when changed tab
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    toolbar.setTitle(adapter.getPageTitle(tab.getPosition()));
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            getSupportActionBar().setTitle(adapter.getPageTitle(0));

        } else {
            navigationView.inflateHeaderView(R.layout.nav_before_login);
            navigationView.inflateMenu(R.menu.drawer_before_login);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.lecturer_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            Intent intent = new Intent(this, About.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(this, Login.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_logout) {
            new AlertDialog.Builder(this)
                    .setTitle("Logout?")
                    .setMessage("Are you sure want to logout?")
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //stop service
                            stopService(new Intent(getBaseContext(), LocationUpdater.class));

                            //logout
                            SharedPreferences sharedPreferences = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(Home.this, Home.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        } else if (id == R.id.nav_setting){
            Intent intent = new Intent(this, Settings.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_editprofile) {
            Intent intent = new Intent(this, EditAccount.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setUserData() {
        String apiUrl = getResources().getString(R.string.api_url);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
        LecturerApi api = restAdapter.create(LecturerApi.class);
        api.getLecturerDetail(String.valueOf(userId), new Callback<LecturerModel>() {
            @Override
            public void success(LecturerModel lecturerModel, Response response) {
                TextView tvName = (TextView) findViewById(R.id.textview_name);
                ImageView ivImage = (ImageView) findViewById(R.id.imageview_user_image);
                TextView tvUsername = (TextView) findViewById(R.id.textview_username);

                tvUsername.setText(username);

                tvName.setText(lecturerModel.getName());

                Image.setImage(Home.this, ivImage, lecturerModel.getPhoto());

                final int userId = lecturerModel.getUserId();
                final String name = lecturerModel.getName();

                LinearLayout sidebarProfile = (LinearLayout) findViewById(R.id.sidebar_profile);
                sidebarProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Home.this, LecturerInfo.class);
                        intent.putExtra("lecturerId", userId);
                        intent.putExtra("lecturerName", name);

                        startActivity(intent);
                    }
                });

                //save data for easier call
                SharedPreferences sharedPreferences = getSharedPreferences("dosenkuData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("userId", lecturerModel.getUserId());
                editor.putString("name", lecturerModel.getName());
                editor.putString("email", lecturerModel.getEmail());
                editor.putString("phone", lecturerModel.getPhone());
                editor.putString("photo", lecturerModel.getPhoto());
                editor.putInt("updateInterval", lecturerModel.getUpdateInterval());
                editor.apply();

                startService(new Intent(getBaseContext(), LocationUpdater.class));
            }

            @Override
            public void failure(RetrofitError error) {
                /*
                Snackbar.make(rootView, R.string.error_fetch, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Refresh", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setUserData();
                            }
                        })
                        .show();
                        */
                setUserData();
            }
        });
    }
}
