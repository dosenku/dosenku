package com.dosenku.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LecturerModel {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("update_interval")
    @Expose
    private Integer updateInterval;
    @SerializedName("is_deleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("lastSeen")
    @Expose
    private Integer lastSeen;
    @SerializedName("place")
    @Expose
    private String place;


    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The updateInterval
     */
    public Integer getUpdateInterval() {
        return updateInterval;
    }

    /**
     *
     * @param updateInterval
     * The update_interval
     */
    public void setUpdateInterval(Integer updateInterval) {
        this.updateInterval = updateInterval;
    }

    /**
     *
     * @return
     * The isDeleted
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     *
     * @param isDeleted
     * The is_deleted
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     *
     * @return
     * The lastSeen
     */
    public Integer getLastSeen() {
        return lastSeen;
    }

    /**
     *
     * @param isDeleted
     * The is_deleted
     */
    public void setLastSeen(Integer lastSeen) {
        this.lastSeen = lastSeen;
    }


    /**
     *
     * @return
     * The place
     */
    public String getPlace() {
        return place;
    }

    /**
     *
     * @param place
     * The place
     */
    public void setPlace(String place) {
        this.place = place;
    }



}