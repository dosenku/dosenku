package com.dosenku.app.model;

/**
 * Created by charl on 11/22/2015.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("is_deleted")
    @Expose
    private Integer isDeleted;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The role
     */
    public Integer getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The isDeleted
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     *
     * @param isDeleted
     * The is_deleted
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

}