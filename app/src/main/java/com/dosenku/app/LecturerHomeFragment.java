package com.dosenku.app;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.setupwizard.SetupWizardStep1;
import com.dosenku.app.utils.Image;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LecturerHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LecturerHomeFragment extends Fragment
    implements SwipeRefreshLayout.OnRefreshListener {
    private int userId;
    private String username;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View rootView;
    private SharedPreferences settings;

    public LecturerHomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LecturerHomeFragment.
     */
    public static LecturerHomeFragment newInstance() {
        LecturerHomeFragment fragment = new LecturerHomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        Configuration config = this.getActivity().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            this.getActivity().getResources().updateConfiguration(config, this.getActivity().getResources().getDisplayMetrics());
        }

        if (getArguments() != null) {

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_lecturer_home, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_lecturer_home);

        //auto start refreshing
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.refresh_1, R.color.refresh_2, R.color.refresh_3, R.color.refresh_4, R.color.refresh_5, R.color.refresh_6);
        swipeRefreshLayout.setOnRefreshListener(this);


        //get lecturer data
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);

        userId = sharedPreferences.getInt("userId", 0);
        username = sharedPreferences.getString("username", "");


        loadData();

        return rootView;
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    public void loadData() {
        String apiUrl = getResources().getString(R.string.api_url);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
        LecturerApi api = restAdapter.create(LecturerApi.class);
        api.getLecturerDetail(String.valueOf(userId), new Callback<LecturerModel>() {
            @Override
            public void success(LecturerModel lecturerModel, Response response) {
                swipeRefreshLayout.setRefreshing(false);
                ScrollView sv  = (ScrollView) rootView.findViewById(R.id.scrollview);
                sv.setVisibility(View.VISIBLE);

                final int userId = lecturerModel.getUserId();

                //set name
                final String name = lecturerModel.getName();
                TextView tvName = (TextView) sv.findViewById(R.id.tv_name);
                tvName.setText(name);

                //set photo
                ImageView ivPhoto = (ImageView) sv.findViewById(R.id.iv_photo);
                Image.setImage(LecturerHomeFragment.this.getActivity(), ivPhoto, lecturerModel.getPhoto());


                //set click to load profile
                LinearLayout layoutProfile = (LinearLayout) sv.findViewById(R.id.linear_profile);
                layoutProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Activity activity = LecturerHomeFragment.this.getActivity();

                        Intent intent = new Intent(activity, LecturerInfo.class);
                        intent.putExtra("lecturerId", userId);
                        intent.putExtra("lecturerName", name);

                        activity.startActivity(intent);
                    }
                });

                Button btnPlace = (Button) sv.findViewById(R.id.btn_place);
                btnPlace.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Activity activity = LecturerHomeFragment.this.getActivity();

                        Intent intent = new Intent(activity, PlaceChooser.class);
                        activity.startActivity(intent);
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                swipeRefreshLayout.setRefreshing(false);
                if (error.getKind().equals(RetrofitError.Kind.HTTP) && (error.getResponse().getStatus() == 404)) {
                    Intent intent = new Intent(getActivity(), SetupWizardStep1.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });
    }
}
