package com.dosenku.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dosenku.app.api.UserApi;
import com.dosenku.app.model.UserLoginModel;
import com.dosenku.app.model.UserModel;

import java.util.Locale;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Login extends AppCompatActivity {
    private Button buttonLogin;
    private EditText etUsername, etPassword;
    private View rootView;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }


        rootView = findViewById(R.id.scrollview_login);

        buttonLogin = (Button) findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        etUsername = (EditText) findViewById(R.id.et_username);

        etPassword = (EditText) findViewById(R.id.et_password);


    }

    private void login() {
        if (!validateInput()) {
            loginFailed();
            return;
        }

        buttonLogin.setEnabled(false);

        final ProgressDialog pd = new ProgressDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        pd.setIndeterminate(true);
        pd.setMessage("Authenticating...");
        pd.show();

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        String apiUrl = this.getResources().getString(R.string.api_url);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();

        UserApi api = restAdapter.create(UserApi.class);

        api.login(username, password, new Callback<UserLoginModel>() {
            @Override
            public void success(UserLoginModel userLoginModel, Response response) {
                pd.dismiss();
                if (userLoginModel.getCode() == 0) {
                    //kalau sukses
                    //loginSuccess();
                    String[] loginData= userLoginModel.getMessage().split(Pattern.quote("|"));

                    String userId = loginData[0];
                    String role = loginData[1];
                    String username = loginData[2];

                    SharedPreferences sharedPreferences = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("userId", Integer.valueOf(userId));
                    editor.putInt("userRole", Integer.valueOf(role));
                    editor.putString("username", username);
                    editor.apply();

                    loginSuccess();
                } else {
                    //kalau gagal
                    Snackbar.make(rootView, userLoginModel.getMessage(), Snackbar.LENGTH_LONG).show();
                    loginFailed();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(rootView, R.string.error_fetch, Snackbar.LENGTH_LONG).show();
                loginFailed();
                pd.dismiss();
            }
        });


    }

    private void loginFailed() {
        buttonLogin.setEnabled(true);
    }

    private void loginSuccess() {
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    private boolean validateInput() {
        boolean valid = true;

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if (username.isEmpty()) {
            etUsername.setError("Enter a username");
            valid = false;
        } else {
            etUsername.setError(null);
        }

        if (password.isEmpty()) {
            etPassword.setError("Enter a password");
            valid = false;
        } else {
            etPassword.setError(null);
        }

        return valid;

    }
}
