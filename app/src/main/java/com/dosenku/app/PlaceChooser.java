package com.dosenku.app;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dosenku.app.api.ActivityApi;
import com.dosenku.app.api.PlaceApi;
import com.dosenku.app.model.ActivityModel;
import com.dosenku.app.model.PlaceModel;
import com.dosenku.app.utils.Helpers;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PlaceChooser extends AppCompatActivity {
    private SharedPreferences settings;
    private RelativeLayout progress;
    private ListView lvPlace;
    private View rootView;

    private SharedPreferences spData;
    private int dosenId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_chooser);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        progress = (RelativeLayout) findViewById(R.id.container_progress);
        lvPlace = (ListView) findViewById(R.id.lv_place);
        rootView = (View) findViewById(R.id.rootview);

        spData = getSharedPreferences("dosenkuData", MODE_PRIVATE);

        dosenId = spData.getInt("userId", 0);

        getData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void getData() {
        String apiUrl = getResources().getString(R.string.api_url);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
        PlaceApi api = adapter.create(PlaceApi.class);

        api.get(new Callback<ArrayList<PlaceModel>>() {
            @Override
            public void success(ArrayList<PlaceModel> placeModels, Response response) {
                progress.setVisibility(View.GONE);
                lvPlace.setVisibility(View.VISIBLE);

                ArrayList<PlaceObject> placeList = new ArrayList<>();

                for (PlaceModel model : placeModels) {
                    PlaceObject obj = new PlaceObject(model.getId(), model.getName());
                    placeList.add(obj);
                }

                PlaceAdapter placeAdapter = new PlaceAdapter(PlaceChooser.this, placeList);

                lvPlace.setAdapter(placeAdapter);
                lvPlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        PlaceObject obj = (PlaceObject) parent.getItemAtPosition(position);

                        updatePlace(obj.getId());
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(rootView, R.string.error_fetch, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Refresh", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getData();
                            }
                        })
                        .show();
            }
        });
    }

    public void updatePlace(int placeId) {
        String apiUrl = getResources().getString(R.string.api_url);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();
        ActivityApi api = adapter.create(ActivityApi.class);

        final ProgressDialog pd = new ProgressDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        pd.setIndeterminate(true);
        pd.setMessage(getResources().getString(R.string.dialog_save_changes));
        pd.show();

        api.sendWithPlace(dosenId, Helpers.getCurrentDateTime(), 0, 0, placeId, new Callback<ActivityModel>() {
            @Override
            public void success(ActivityModel activityModel, Response response) {
                pd.dismiss();
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Snackbar.make(rootView, R.string.error_save, Snackbar.LENGTH_INDEFINITE)
                        .show();
                pd.dismiss();
            }

        });
    }


    private class PlaceObject {
        private int id;
        private String name;

        PlaceObject(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    private class PlaceAdapter extends BaseAdapter {
        private ArrayList<PlaceObject> placeList;
        private Activity parent;
        private LayoutInflater inflater;

        PlaceAdapter(Activity parent, ArrayList<PlaceObject> list) {
            this.parent = parent;
            placeList = list;
            inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return placeList.size();
        }

        @Override
        public Object getItem(int position) {
            return placeList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = inflater.inflate(R.layout.item_place, null);
            }

            TextView name = (TextView) view.findViewById(R.id.item_name);
            PlaceObject obj = placeList.get(position);

            name.setText(obj.getName());


            return view;
        }
    }
}
