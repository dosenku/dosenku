package com.dosenku.app.setupwizard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dosenku.app.R;

import java.util.Locale;

public class SetupWizardStep2 extends AppCompatActivity {
    private EditText etPassword, etConfirmPassword;
    private String username;
    private SharedPreferences settings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wizard_step2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        //etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);

        SharedPreferences sharedPreferences = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE);
        username = sharedPreferences.getString("username", "");


        //etUsername.setText(username);
        TextView tvUsername = (TextView) findViewById(R.id.tv_username);
        tvUsername.setText(username);

        Button btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput()) {
                    //String username = etUsername.getText().toString();
                    String password = etPassword.getText().toString();

                    Intent intent = new Intent(SetupWizardStep2.this, SetupWizardStep3.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validateInput() {
        boolean valid = true;

        //String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
/*
        if (username.isEmpty()) {
            etUsername.setError("Enter a username");
            valid = false;
        } else {
            etUsername.setError(null);
        }
*/
        if (password.isEmpty()) {
            etPassword.setError("Enter a password");
            valid = false;
        } else {
            etPassword.setError(null);
        }

        if (confirmPassword.isEmpty()) {
            etConfirmPassword.setError("Enter the password again");
            valid = false;
        } else {
            if (!password.equals(confirmPassword)) {
                etConfirmPassword.setError("Passwords do not match");
                valid = false;
            } else {
                etPassword.setError(null);
            }

        }

        return valid;

    }
}
