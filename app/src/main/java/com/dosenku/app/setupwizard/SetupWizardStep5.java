package com.dosenku.app.setupwizard;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dosenku.app.Home;
import com.dosenku.app.R;
import com.dosenku.app.api.LecturerApi;
import com.dosenku.app.model.LecturerModel;
import com.dosenku.app.utils.FileUtils;
import com.dosenku.app.utils.Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class SetupWizardStep5 extends AppCompatActivity {
    private String username, password, name, email, phone, photo;
    private static final int IMAGE_REQUEST_CODE = 1;
    private ImageView ivPhoto;
    private View rootView;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wizard_step5);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        rootView = (LinearLayout) findViewById(R.id.rootview);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            username = extras.getString("username");
            password = extras.getString("password");
            name = extras.getString("name");
            phone = extras.getString("phone");
            email = extras.getString("email");
            photo = extras.getString("photo");
        } else {
            username = savedInstanceState.getString("username");
            password = savedInstanceState.getString("password");
            name = savedInstanceState.getString("name");
            phone = savedInstanceState.getString("phone");
            email = savedInstanceState.getString("email");
            photo = savedInstanceState.getString("photo");
        }

        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        try {
            ivPhoto.setImageBitmap(Image.decodeUri(this, Uri.parse(photo), 200));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        TextView tvUsername = (TextView) findViewById(R.id.tv_username);
        TextView tvPassword = (TextView) findViewById(R.id.tv_password);
        TextView tvName = (TextView) findViewById(R.id.tv_name);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvEmail = (TextView) findViewById(R.id.tv_email);


        tvUsername.setText(username);
        tvPassword.setText("******");
        tvName.setText(name);
        tvEmail.setText(email);
        tvPhone.setText(phone);


        Button btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createData();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("username", this.username);
        savedInstanceState.putString("password", this.password);
        savedInstanceState.putString("name", this.name);
        savedInstanceState.putString("phone", this.phone);
        savedInstanceState.putString("email", this.email);
        savedInstanceState.putString("photo", this.photo);
    }

    private void createData() {
        //progress dialog
        final ProgressDialog pd = new ProgressDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        pd.setIndeterminate(true);
        pd.setMessage(getString(R.string.dialog_save_changes));
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();


        String apiUrl = getResources().getString(R.string.api_url);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(apiUrl).build();

        LecturerApi api = restAdapter.create(LecturerApi.class);

        Log.e("step4", photo);

        TypedFile filePhoto = null;
        try {
            filePhoto = FileUtils.getTypedFileFromUri(this, android.net.Uri.parse(new java.net.URI(photo).toString()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        int userId = getSharedPreferences("dosenkuLogin", Context.MODE_PRIVATE).getInt("userId", 0);
        api.createLecturer(userId, name, phone, email, filePhoto, password, new Callback<LecturerModel>() {
            @Override
            public void success(LecturerModel lecturerModel, Response response) {
                pd.dismiss();
                createSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                Snackbar.make(rootView, R.string.error_save, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void createSuccess() {
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
