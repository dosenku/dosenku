package com.dosenku.app.setupwizard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dosenku.app.R;

import java.util.Locale;

public class SetupWizardStep3 extends AppCompatActivity {
    private String username, password;
    private EditText etName, etPhone, etEmail;
    private SharedPreferences settings;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wizard_step3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        //ambil dari intent
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            username = extras.getString("username");
            password = extras.getString("password");
        } else {
            username = savedInstanceState.getString("username");
            password = savedInstanceState.getString("password");
        }

        etName = (EditText) findViewById(R.id.et_name);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etEmail = (EditText) findViewById(R.id.et_email);


        Button btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput()) {
                    String name = etName.getText().toString();
                    String phone = etPhone.getText().toString();
                    String email = etEmail.getText().toString();

                    Intent intent = new Intent(SetupWizardStep3.this, SetupWizardStep4.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    intent.putExtra("name", name);
                    intent.putExtra("phone", phone);
                    intent.putExtra("email", email);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("username", this.username);
        savedInstanceState.putString("password", this.password);
    }

    private boolean validateInput() {
        boolean valid = true;

        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();
        String email = etEmail.getText().toString();

        if (name.isEmpty()) {
            etName.setError("Enter a name");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (phone.isEmpty()) {
            etPhone.setError("Enter a phone number");
            valid = false;
        } else {
            etPhone.setError(null);
        }

        if (email.isEmpty()) {
            etEmail.setError("Enter an email address");
            valid = false;
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                etEmail.setError(null);
            } else {
                etEmail.setError("Enter a valid email address");
                valid = false;
            }

        }

        return valid;
    }
}
