package com.dosenku.app.setupwizard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.dosenku.app.R;
import com.dosenku.app.utils.Image;

import java.io.FileNotFoundException;
import java.util.Locale;

public class SetupWizardStep4 extends AppCompatActivity {
    private String username, password, name, email, phone, photo;
    private static final int IMAGE_REQUEST_CODE = 1;
    private ImageView ivPhoto;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_wizard_step4);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            username = extras.getString("username");
            password = extras.getString("password");
            name = extras.getString("name");
            phone = extras.getString("phone");
            email = extras.getString("email");
        } else {
            username = savedInstanceState.getString("username");
            password = savedInstanceState.getString("password");
            name = savedInstanceState.getString("name");
            phone = savedInstanceState.getString("phone");
            email = savedInstanceState.getString("email");
        }

        ivPhoto = (ImageView) findViewById(R.id.iv_photo);

        //init the photo variable
        photo = "";

        Button btnPickPhoto = (Button) findViewById(R.id.btn_pick_photo);
        btnPickPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

                startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
            }
        });


        Button btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photo.isEmpty()) {
                    Snackbar.make(v, "Please select a photo", Snackbar.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(SetupWizardStep4.this, SetupWizardStep5.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    intent.putExtra("name", name);
                    intent.putExtra("phone", phone);
                    intent.putExtra("email", email);
                    intent.putExtra("photo", photo);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("username", this.username);
        savedInstanceState.putString("password", this.password);
        savedInstanceState.putString("name", this.name);
        savedInstanceState.putString("phone", this.phone);
        savedInstanceState.putString("email", this.email);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                Uri selectedImageUri = data.getData();

                ivPhoto.setImageBitmap(null);
                try {
                    ivPhoto.setImageBitmap(Image.decodeUri(this, selectedImageUri, 200));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                photo = selectedImageUri.toString();
            }
        }
    }
}
